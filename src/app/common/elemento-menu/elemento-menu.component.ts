import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {UtilsService} from '../../services/utils.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-elemento-menu',
  templateUrl: './elemento-menu.component.html',
  styleUrls: ['./elemento-menu.component.scss']
})
export class ElementoMenuComponent implements OnInit {

  _textoBase = '';
  @Input()
  get textoBase(): string {
    return this._textoBase;
  }
  set textoBase(valor: string) {
    this._textoBase = valor;
    this.refresh();
  }

  @Output()
  seleccion = new EventEmitter<string>();

  texto: string;

  constructor(
    public utilsService: UtilsService,
  ) { }

  ngOnInit(): void {
  }

  refresh(): void {
    this.texto = String(Math.round(Math.random() * 1000));
  }

  select(): void {
    this.seleccion.emit(this.texto);
  }
}
