import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  listaDatos = [
    'Mis ahorros',
    'Mis deudas',
    'Mis ingresos',
    'Mis impuestos',
  ];
  detailSelected: string;

  constructor() { }

  ngOnInit(): void {
  }

  onSelected(value: string): void {
    this.detailSelected = value;
  }

  isEven(rowNumber: number): boolean{
    return rowNumber % 2 === 0;
  }

}
