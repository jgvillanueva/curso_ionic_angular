import {Component, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {UtilsService} from '../../services/utils.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  @Input()
  detail: string;

  constructor(
    private utilsService: UtilsService
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { detail } = changes;
    if (detail.previousValue !== detail.currentValue) {
      console.log('Detectado cambio en detail', detail.currentValue);
    }
  }

}
