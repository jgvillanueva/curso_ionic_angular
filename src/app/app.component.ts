import {Component, Input, Output} from '@angular/core';
import {UtilsService} from './services/utils.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'angular-app';

  constructor(
    public utilsService: UtilsService,
  ) {}
}
