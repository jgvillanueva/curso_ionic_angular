import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Route, Router} from '@angular/router';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  datos = [
    {
      type: 'Email',
      valor: 'jorgegvillanueva@gmail.com'
    },
    {
      type: 'Teléfono',
      valor: '666555444'
    },
    {
      type: 'Dirección',
      valor: 'Gran vía 16'
    },
  ]
  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
  }

  openData(objeto): void {
   this.router.navigate(['data/' + objeto.type], { relativeTo: this.route, state: { datos: objeto } });
  }
}
