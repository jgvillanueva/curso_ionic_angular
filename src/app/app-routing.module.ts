import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MenuComponent} from './menu/menu.component';
import {AboutComponent} from './common/about/about.component';
import {DataComponent} from './common/data/data.component';
import {InfoComponent} from './common/info/info.component';

const routes: Routes = [
  { path: '', component: MenuComponent },
  { path: 'about', component: AboutComponent, children: [
      { path: 'data/:type', component: DataComponent },
      { path: 'info', component: InfoComponent },
    ] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
