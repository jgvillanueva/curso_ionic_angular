import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { ElementoMenuComponent } from './common/elemento-menu/elemento-menu.component';
import { DetailComponent } from './common/detail/detail.component';
import { UpperwordPipe } from './pipes/upperword.pipe';
import { AboutComponent } from './common/about/about.component';
import { DataComponent } from './common/data/data.component';
import { InfoComponent } from './common/info/info.component';
import {FormsModule} from '@angular/forms';

registerLocaleData(localeEs, 'es');

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ElementoMenuComponent,
    DetailComponent,
    UpperwordPipe,
    AboutComponent,
    DataComponent,
    InfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'es' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
