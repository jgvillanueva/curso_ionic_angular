import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  valorcampo
  static ARROW = '->';

  constructor() {

  }

  addArrow(value: string): string {
    return UtilsService.ARROW + ' ' + value;
  }
}
