import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'upperword'
})
export class UpperwordPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    let list = value.split(' ');
    list = list.map((word: string) => {
      return word.substr(0, 1).toUpperCase() + word.substr(1);
    })
    return list.join(' ');
  }

}
