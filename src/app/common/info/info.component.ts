import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {UtilsService} from '../../services/utils.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
  miCampo

  constructor(
    private utilsService: UtilsService
  ) { }

  ngOnInit(): void {
  }

  onChange(): void {
    this.utilsService.valorcampo = this.miCampo;
  }
}
