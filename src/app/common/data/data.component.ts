import {Component, OnDestroy, OnInit, Provider} from '@angular/core';
import {ActivatedRoute, ParamMap, Route, Router} from '@angular/router';
import {Location} from '@angular/common';
import {Subscriber, Subscription} from 'rxjs';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent implements OnInit, OnDestroy {
  type
  datos
  paramSuscription: Subscription

  constructor(
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    // observable para que pueda cambiar
    this.paramSuscription = this.route.paramMap.subscribe(params => {
      this.type = params.get('type');
      this.datos = history.state.datos;
    });
  }

  ngOnDestroy(): void {
    this.paramSuscription.unsubscribe();
  }

}
